import React from 'react';
import MainPage from './pages/main_page';
import ContSlyder from './pages/design_page';
import OurProjects from "./pages/our_prjects";
import PageContacts from './pages/contacts_pages'
import './Reset.css';
import './App.css';
import './media.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const App = () => {
  return (
    <div>
    <Router>
      <header className="conteinerHeader"> 
      <div className="conteinerLogo">
      
        <Link to="/">AGENCY</Link>
     
      </div>
     
        <nav>
          <ul className="navigation">
            <li>
              <Link to="/">Главная</Link>
            </li>
            
            <li>
              <Link to="/pages/design_page">Дизайн</Link>
            </li>
            <li>
              <Link to="/pages/our_works">Наши проэкты</Link>
            </li>
            <li>
              <Link to="/pages/contacts_pages">Контакты</Link>
            </li>
        </ul>
      </nav>
      
      <div> 
        
      </div>
      </header>
      <Route exact path="/" component={MainPage} />
      <Route path="/pages/main_page" component={MainPage} />
      <Route path="/pages/design_page" component={ContSlyder} />
      <Route path="/pages/our_works" component={OurProjects} />
      <Route path="/pages/contacts_pages" component={PageContacts} />


      </Router>
      
      
     
    </div>
  )
}

export default App;
