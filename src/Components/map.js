import React from 'react';

const ContMap = () => {
    return (
        <div className='conteinerMap'>
            <div className='map'>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d164152.7688214594!2d36.14574292830755!3d49.99472774437151!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a09f63ab0f8b%3A0x2d4c18681aa4be0a!2z0KXQsNGA0YzQutC-0LIsINCl0LDRgNGM0LrQvtCy0YHQutCw0Y8g0L7QsdC70LDRgdGC0Yw!5e0!3m2!1sru!2sua!4v1561556968290!5m2!1sru!2sua" width="100%"  frameborder="0"  allowfullscreen></iframe>
            </div> 
            <div className='contLocalization'>
                <div className='localiztion'>
                    <div className='localiztionIcons'>
                        <img src="/images/iconLocalization.png" alt="localization"/>
                    </div>
                    <div className='localiztionInfo'>
                        <p>
                            <strong>Адресс:</strong><br/>
                            г. Харьков ул. Первомайская 56б
                        </p>
                    </div>
                </div>
                <div className='localiztion'>
                    <div className='timeIcons'>
                        <img src="/images/time.png" alt="localization"/>
                    </div>
                    <div className='localiztionInfo'>
                        <p>
                            <strong>График работы:</strong> с пн -вс <br/>
                            с 9:00 - до последнего
                        </p>
                    </div>
                </div>
                    
            </div>
        </div>
    )
}
export default ContMap