import React, {Component} from 'react';
import Logo from './Logo';

class ConteinerLogo extends Component {
    state = {
    logo : [
    {src:'/images/Object.png'},
    {src:'/images/Object1.png'},
    {src:'/images/Object2.png'},
    {src:'/images/Object3.png'},
    {src:'/images/Object4.png'},
    {src:'/images/Object5.png'}
]
}
render() {
    return (
        <div className='logoCont'>
            {this.state.logo.map(item => {
                return <Logo logo = {item.src} />
            })}
            {/* <Logo props={props} /> */}
        </div>
    )
    }
}   
export default ConteinerLogo