import React from 'react';
import SendMassageForm from './cont_form'

const ContMessageForm = () => {
    return (
        <div className='conteinerContactInfo'>
            
            <h3 className='contactsTitle'>
                Конакты
            </h3>
            <p className='contactSubTitle'>
                Ответим на все ваши вопросы. По вопросам 
                дизайна различных сложностях вы сможете позвонить 
                по телефону 8(098) 675 45 56 . Или просто заполните форму ниже.
            </p>
            <h6 className='titleForm'>
                Обратная связь 
            </h6>
          
            <SendMassageForm/>
        </div>
    )
}
export default ContMessageForm