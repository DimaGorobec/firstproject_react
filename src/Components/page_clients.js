import React from 'react'
import ConteinerLogo from './Company_logo'


const ContClient= () => {
    return (
        <div className='conteinerClients'>
            <h3>Наши клиенты</h3>  
            <div className="contYear">
                <img src="/images/Group.png" alt=""/>
            </div>
            <ConteinerLogo/>
        </div>
    )
}
export default ContClient