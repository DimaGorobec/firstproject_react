import React, {Component} from 'react';
import SlyderWrapper from './Sluder_prod'
import Project from "./project";


class ListFoto extends Component {
    state = {
        fotos: [
            {
               mainFoto: '/images/image16.png',
               cityTitle: 'Строительство в Харькове',
               gallary: [
                '/images/image16.png',
                '/images/image12.png',
                '/images/-FilesZ500-res-wizualizacje-Zx183-Zx183_view1_jpg.jpg',
                '/images/-FilesZ500-res-wizualizacje-Zx183-Zx183_view2_jpg.jpg',
                '/images/minmalizm1.jpg',
                ]
            },
            {
                mainFoto: '/images/image14.1.png',
                cityTitle: 'Строительство в Одессе',
                gallary: [
                    '/images/image16.png',
                '/images/image12.png',
                '/images/-FilesZ500-res-wizualizacje-Zx183-Zx183_view1_jpg.jpg',
                '/images/-FilesZ500-res-wizualizacje-Zx183-Zx183_view2_jpg.jpg',
                '/images/minmalizm1.jpg',
                ]
             },
             {
                mainFoto: '/images/image12.png',
                cityTitle: 'Строительство в Львове',
                gallary: [
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                ]
             },
             {
                mainFoto: '/images/image12.png',
                cityTitle: 'Строительство в Львове',
                gallary: [
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                ]
             },
             {
                mainFoto: '/images/image12.png',
                cityTitle: 'Строительство в Львове',
                gallary: [
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                ]
             },
             {
                mainFoto: '/images/image12.png',
                cityTitle: 'Строительство в Львове',
                gallary: [
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                    '/images/image12.png',
                ]
             },
             {
                mainFoto: '/images/image6.1.png',
                cityTitle: 'Строительство в Киеве',
                gallary: [
                    '/images/image6.1.png',
                    '/images/image6.1.png',
                    '/images/image6.1.png',
                    '/images/image6.1.png',
                    '/images/image6.1.png',
                ]
             }
        ]
    } 
    render() {
       
        return (
            <div className = " slyderWrapper">
                <div className='projectTitle'>
                    <h3>Наши проекты</h3>
                </div>
                <SlyderWrapper props = {this.state.fotos} handler = {this.state.hendler}>
                </SlyderWrapper>
            </div>
        )
    }
    
}
export default  ListFoto