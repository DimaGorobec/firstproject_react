import React, {Component} from 'react';

class SendMassageForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {name: '', email: '', massage: ''};
      this.onChangeName = this.onChangeName.bind(this);
      this.onChangeEmail = this.onChangeEmail.bind(this);
      this.onChangeMassage = this.onChangeMassage.bind(this);

      this.onSubmit = this.onSubmit.bind(this);
    }
    onSubmit(event){ 
      event.preventDefault();
      if(this.state.name.length > 0 && this.state.email.length > 0 && this.state.massage.length > 0) {
        alert(`${this.state.name}, получил ваше сообщение!`);
        this.setState({email: '', massage: '',name: ''})
      } else {
        return false;
      }
    }
    onChangeEmail(event){
      this.setState({email: event.target.value});
    }
    onChangeName(event) {
      this.setState({name: event.target.value});
    }
    onChangeMassage(event) {
        this.setState({massage: event.target.value});
    }
    render() {   
      return (
          <div>
            <form onSubmit={this.onSubmit} className='conteinerForm'>
                <p><input className='input' type="text" autocomplete = 'off' name="name" value={this.state.name} placeholder='Имя'
                            onChange={this.onChangeName}/></p>
                <p><input className='input' type="email" name="email" value={this.state.email} placeholder = 'E-mail'
                                onChange={this.onChangeEmail}/></p>
                <p><textarea name="massage" value={this.state.massage} placeholder='Сообщение'
                                onChange={this.onChangeMassage}/></p>
            <p><input className='submitBtn' type="submit" value="Отправить" /></p>
            </form>
        </div>
      );
    }
  }
export default SendMassageForm