import React from 'react';

const Slydes = (props) => {
    
    return (
        <div className="slyde"> 
            <div className='textConteiner'>
                <h3 className='slydeTitle'>
                    {props.title}    
                </h3>
                <p className='slydeSubTitle'>
                    {props.subTitle}
                </p>
            </div>
            <div className='secondFoto'>
                <img src= {props.secondFoto} alt=""/>
            </div>
            <div className='firstFoto'>
                <img src= {props.mainImg} alt=""/>
            </div>
        </div>
    )
}
export default Slydes