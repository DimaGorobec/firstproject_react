import React from 'react';
import Project from "./project";
import Slider from "react-slick";
import Slydes from '../Components/slydes';

class SlyderWrapper extends React.Component {
    render(props) {
      
      const arr = this.props.props;
      let settings = {
        dots: false,
        arrows: true,
        infinite: true,
        speed: 500,
        autoplay: 2000,
        slidesToShow: 4,
        slidesToScroll: 1
      };
      
      return (
        
          <div className=''>
              <div class='projectSlyder'>   
                <Slider {...settings}>
                   {arr.map((item) => {
                        return <Project gallary= {item.gallary} cityTitle = {item.cityTitle} mainFoto = {item.mainFoto} />
                    })}
                </Slider>
            </div>
          </div>
      );
    }
  }

export default SlyderWrapper