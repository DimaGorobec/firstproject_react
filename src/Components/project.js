import React, {Component} from 'react';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

 class Project extends React.Component {
    constructor(props) {
        super(props)
    this.state = {
        isToggleOn: false,
        photoIndex: 0
    }   
    this.hendler = this.hendler.bind(this);
}
hendler()   {
    this.setState(prevState => ({
        isToggleOn: !prevState.isToggleOn
      }));
}
    render () {
        const { photoIndex, isToggleOn } = this.state;
        const galary = this.props.gallary;
        return  (
        <div className='projectCont' >
            <img src={this.props.mainFoto} alt="project"/>
            <h4 className="SlydeTitle">{this.props.cityTitle}</h4>
                <div className='contPlus' onClick= {(e) => {
                    e.preventDefault();
                    this.setState({ isToggleOn: true })}}>
                    {isToggleOn && (
          <Lightbox
            mainSrc={galary[photoIndex]}
            nextSrc={galary[(photoIndex + 1) % galary.length]}
            prevSrc={galary[(photoIndex + galary.length - 1) % galary.length]}
            onCloseRequest={() => this.setState({ isToggleOn: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + galary.length - 1) % galary.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % galary.length,
              })
            }
          />
        )}




                    <img src="/images/zoom_in.png" alt="zoom"/>
                </div>
        </div>
    )
        }
}
export default Project