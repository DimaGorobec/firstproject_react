// import React from 'react';
import React, { Component } from "react";
import Slider from "react-slick";
import Slydes from '../Components/slydes';


const arrayInfo = [
    {
        title: 'Дизайн дома',
        subTitle: "Современный метод строительсва домов, теплиц, бань и множества других конструкций в футуристическом стиле. Это одна из самых прочных и теплых  современных конструкций",
        mainImg: '/images/image14.png',
        secondImg: '/images/image6.png'
    }, 
    {
        title: 'Дом в стиле минимализм ',
        subTitle: "Что представляет собой минимализм?Дизайн интерьера в стиле минимализм предусматривает принцип минимума: - минимальное количество деталей, декора, расцветок, предметов.",
        mainImg: '/images/t3-min.jpg',
        secondImg: '/images/t23-min.jpg'
    },
    {
        title: 'Дом в стиле Прованс',
        subTitle: "Среди всех известных стилей в дизайне интерьера, которые стремятся сделать наши дома уютными и красивыми, стиль Прованс в интерьере загородного дома – несомненно, один из самых легких и воздушных. ",
        mainImg: '/images/10dizajio_office_05-min.jpg',
        secondImg: '/images/stil-provans-v-interere-zagorodnogo-doma-03.jpg'
    }
]

class SimpleSlider extends React.Component {
    render() {
      let settings = {
        dots: true,
        arrows: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        autoplay: 2000,
        slidesToScroll: 1
      };
      return (
          <div className='contSlyder'>
              <div class='slyderWrapper'>   
                <Slider {...settings}>
                    {arrayInfo.map((item) => {
                        return <Slydes title = {item.title} subTitle = {item.subTitle} secondFoto = {item.secondImg} 
                        mainImg = {item.mainImg}  />
                    })}
                </Slider>
            </div>
          </div>
      );
    }
  }

export default SimpleSlider