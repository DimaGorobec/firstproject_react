import React from 'react';
import Slyder from '../Components/page_prod'
import ContClient from '../Components/page_clients'


const OurProjects = () => {
    return (
        <div className='contOusProject'>
            <Slyder/>
            <ContClient/>
        </div>
    )
}
export default OurProjects