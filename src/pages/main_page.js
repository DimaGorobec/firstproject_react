import React from "react";


const MainPage = () => {
    let headerStyle = document.querySelector('.conteinerHeader');
    return (
        <div className="mainPageConteiner">
            <h2 className="mainPageSubTitle">Агентство строительства домов.<br/>
Мы делаем это для вас.</h2>
            <h1 className="mainPageTitle">
                AGENCY
            </h1>
        </div>
    )
}
export default MainPage;