import React from 'react'
import ContMessageForm from '../Components/send_massage';
import ContMap from '../Components/map'

const PageContacts = () => {
    return (
        <div className='PageContact'>
            <div className='slyderWrapper'>
                <div className='contContacts'>
                    <ContMessageForm/>
                    <ContMap/>
                </div>
            </div>
        </div>
    )
}

export default PageContacts